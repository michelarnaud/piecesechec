#ifndef PION_H
#define PION_H

#include "piece.h"

class Pion : public Piece
{
            bool dejajoue;
            Echiquier *mechiquier;
public:

    Pion (Echiquier *echiquier,char a = 'E', char b = '7', char coul = 'b'  );

    /**
     * Empty Destructor
     */
    virtual ~Pion ( );
    bool deplacer(bool *prise,char a, char b=0);
    Pion operator ++(int a);
    Pion operator +=(char a);

};

#endif // PION_H
