#include "pion.h"
#include <QDebug>
#include<iostream>
#include<fstream>
using namespace std;

Pion::Pion (Echiquier *echiquier,char a , char b , char coul  )
:Piece(echiquier,a,b,coul)
{
    dejajoue=false;
    calculerPosition();
    mechiquier=echiquier;
}

Pion::~Pion ( ) { }

bool Pion::deplacer(bool *prise,char a,char b){
for (int i=0;i<64;i++) qDebug() << i<< "  val:"<<mechiquier->getCase(i);
qDebug()<< this->calculerPosition()<< "      "<< mechiquier->getCase(this->calculerPosition());
    *prise=false;
    if ((b>2)||(b<=0)){
        this->calculerPosition();
        return false;
    }
    if ((b==1)and ((a>1)or (a<-1))){
        this->calculerPosition();
        return false;
    }
    if ((b==2)&&(dejajoue)){

      if (((this->y+b)>'8')or((this->y-b)<'1')or((this->x+a)>'H')or((this->x+a)<'A')){
          this->calculerPosition();
          return false;
      }
    }

     if (this->couleur=='b'){
         if (a==0){
             mechiquier->setCase(this->calculerPosition(),vide);
                this->y+=b;
         }
         else { int col=mechiquier->getCase(this->calculerPosition()+a+8);
             if (mechiquier->getCase(this->calculerPosition()+a+8)==noire){
             mechiquier->setCase(this->calculerPosition(),vide);
             this->y+=b;
             this->x+=a;
             mechiquier->setCase(this->calculerPosition(),vide);
             *prise=true;
             }
             else
                 return false;
         }


     }
     else {
         if (a==0){
             int pos=this->calculerPosition();
             mechiquier->setCase(this->calculerPosition(),vide);
             this->y-=b;
         }
         else { int col=mechiquier->getCase(this->calculerPosition()+a-8);
             if (mechiquier->getCase(this->calculerPosition()+a-8)==blanche){
             mechiquier->setCase(this->calculerPosition(),vide);
             this->y-=b;
             this->x+=a;
             mechiquier->setCase(this->calculerPosition(),vide);
             *prise=true;
             }
             else
                 return false;
         }
    }
     dejajoue=true;
     int pos=this->calculerPosition();
     if (this->couleur=='b')
        mechiquier->setCase(pos,blanche);
     else
         mechiquier->setCase(this->calculerPosition(),noire);

     for (int i=0;i<64;i++) qDebug()<< i<< "  val:" << mechiquier->getCase(i);
     qDebug()<< this->calculerPosition()<< "      "<< mechiquier->getCase(this->calculerPosition());

    return true;
}

Pion Pion::operator ++(int a){
 if (this->couleur=='b'){
    if (((this->y+1)>'H')||((this->y+1)<'A'))
      return *this ;
      dejajoue=true;
            this->y++;
 }
       else{
    if (((this->y-1)>'H')||((this->y-1)<'A'))
      return *this ;
      dejajoue=true;
            this->y--;
       }
    return *this;
}

Pion Pion::operator +=(char a){
    if ((a>2)||(a<0))
        return *this;
    if ((a==2)&&(dejajoue))
        return *this;
 if (this->couleur=='b'){
    if (((this->y+a)>'H')||((this->y+a)<'A'))
      return *this ;
    dejajoue=true;
    this->y+=a;
 }
        else {
    if (((this->y-a)>'H')||((this->y-a)<'A'))
      return *this ;
    dejajoue=true;
    this->y-=a;
        }
    return *this;
}
