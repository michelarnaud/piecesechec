#include "piece.h"

Piece::Piece()
{

}

Piece::~Piece ( ) {
//initAttributes();
}

Piece::Piece (Echiquier *echiquier ,char a , char b , char coul   ) {
    this->x=a;
    this->y=b;
    this->couleur=coul;
}

char Piece::getPosX(){
    return this->x;
}

char Piece::getPosY(){
    return this->y;
}


bool Piece::deplacer(char a, char b){
    if (((this->x+a)>'H')||((this->x+a)<'A'))
      return this->calculerPosition();
    if (((this->y-b)>'8')||((this->y-b)<'1'))
      return this->calculerPosition();
    this->x+=a;
    this->y-=b;
    this->calculerPosition();
    return true;
}

char Piece::getCouleur(){
    return this->couleur;
}

int Piece::calculerPosition(){
    int position=((int)8*(y-0x31)+(int)(x-0x41));
    return position;
}

Piece  Piece::operator +=(char a){
     if (((this->x+a)>'8')||((this->x+a)<'1'))
      return *this;
    if (((this->y+a)>'H')||((this->y+a)<'A'))
      return *this ;
    this->x+=a;
    this->y+=a;
    return *this;
}

