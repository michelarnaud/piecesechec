#ifndef PIECE_H
#define PIECE_H
#include "echiquier.h"

class Piece
{
protected:
    char couleur;
    char x;
    char y;

public:
    Piece();
    Piece ( Echiquier *echiquier,char a = 'C', char b = '7', char coul = 'b' );
   virtual ~Piece ( );
     Piece  operator +=(char n) ;
     Piece operator -=(char n);
     Piece operator >>=(char n);
     Piece operator <<=(char n);
     bool deplacer (char a, char b );
     int calculerPosition();
    char getPosX();
    char getPosY();
    char getCouleur();
};

#endif // PIECE_H
